# multisock

This crate provides unified `SocketAddr`, `Stream` and `Listener` types that work with both TCP
and UNIX sockets.

Many applications don't really care whether they are connecting to a UNIX or TCP service, they
simply want to use the service. Similarly, applications may want to provide a service over
either a UNIX socket or TCP port. The difference between these two socket types matters as much
to application logic as the difference between IPv4 and IPv6 - not that much, typically. Yet
libstd provides a unified type for IPv4 and IPv6 sockets, but requires a separate type for UNIX
sockets. The types provided by this crate allow for writing socket-type-agnostic network
applications that treat UNIX sockets in the same way as IPv4 and IPv6: Just a matter of
run-time configuration.

These types should behave the same as the `SocketAddr`, `TcpStream`/`UnixStream` and
`TcpListener`/`UnixListener` in libstd. There is currently no support for mio or tokio.

UDP and Datagram sockets are not currently supported.

On Windows, these types only support TCP and are just lightweight wrappers around TCP sockets.

# Client example

```rust,no_run
use multisock::{SocketAddr,Stream};
use std::io::{Write,Result};

// Send the pause command to MPD.
fn mpd_pause(addr: &SocketAddr) -> Result<()> {
    let mut sock = Stream::connect(addr)?;
    sock.write_all(b"pause 1\r\n")?;
    Ok(())
}

// If MPD is listening on a UNIX socket, we can do this:
mpd_pause(&"unix:/var/lib/mpd/socket".parse().unwrap())?;

// Or if it's running on another system in the network:
mpd_pause(&"10.3.3.7:6600".parse().unwrap())
```
